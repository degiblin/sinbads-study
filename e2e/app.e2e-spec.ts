import { SinbadsStudyPage } from './app.po';

describe('sinbads-study App', function() {
  let page: SinbadsStudyPage;

  beforeEach(() => {
    page = new SinbadsStudyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
