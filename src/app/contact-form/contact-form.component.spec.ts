/* tslint:disable:no-unused-variable */
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { ContactFormComponent } from './contact-form.component';

describe('ContactFormComponent', () => {
  let component: ContactFormComponent;
  let fixture: ComponentFixture<ContactFormComponent>;

  let page: Page;

  class Page {
    /** SUT HTML native elements */

    form: HTMLFormElement;
    emailLabel: HTMLLabelElement;
    emailInput: HTMLInputElement;
    messageLabel: HTMLLabelElement;
    messageInput: HTMLTextAreaElement;
    submitButton: HTMLButtonElement;
    httpSpy: jasmine.Spy;

    constructor() {
    }

    /** Add page elements after input arrives */
    addPageElements() {
      // if (component.input) {
      // have input so these elements are now in the DOM
      this.form = fixture.debugElement.query(By.css('form')).nativeElement;
      this.emailLabel = fixture.debugElement.query(By.css('div.form-group>label[for="#emailInput"]')).nativeElement;
      this.emailInput = fixture.debugElement.query(By.css('div.form-group>#emailInput.form-control')).nativeElement;
      this.messageLabel = fixture.debugElement.query(By.css('div.form-group>label[for="#messageInput"]')).nativeElement;
      this.messageInput = fixture.debugElement.query(By.css('div.form-group>#messageInput.form-control')).nativeElement;
      this.submitButton = fixture.debugElement.query(By.css('form>button[type="submit"].btn.btn-primary')).nativeElement;
      // }
    }
  }
  function createComponent() {

    /** Service stubs */
    let httpService;

    TestBed.configureTestingModule({
      imports: [FormsModule, HttpModule],
      declarations: [ContactFormComponent],
      providers: []
    })
      .compileComponents();

    fixture = TestBed.createComponent(ContactFormComponent);
    component = fixture.componentInstance;
    page = new Page();
    httpService = fixture.debugElement.injector.get(Http);
    // Setup spy on the `getQuote` method
    page.httpSpy = spyOn(httpService, 'post');

    /** Inputted Property Initialization */

    // component.property = {}



    // 1st change detection triggers ngOnInit
    fixture.detectChanges();
    return fixture.whenStable().then(() => {
      // 2nd change detection displays the async
      fixture.detectChanges();
      page.addPageElements();
    });
  }



  beforeEach(async(() => {
    createComponent();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a form', () => {
    expect(page.form).toBeTruthy();
  });

  it('should render email form group', () => {
    expect(page.emailInput).toBeTruthy();
    expect(page.emailLabel).toBeTruthy();
  });

  it('should render message form group', () => {
    expect(page.messageInput).toBeTruthy();
    expect(page.messageLabel).toBeTruthy();
  });

  it('should render submit button', () => {
    expect(page.submitButton.textContent).toContain("Send");
  });

  it('should call submitMessage and http.post', async(() => {

    let spy = spyOn(component, 'submitMessage');


    let expectedEmail = "test@email.com";
    let expectedMessage = "Test message.";

    page.emailInput.value = expectedEmail;
    page.emailInput.dispatchEvent(new Event('input'));
    page.messageInput.value = expectedMessage;
    page.messageInput.dispatchEvent(new Event('input'));

    page.submitButton.click();


    fixture.whenStable().then(() => {
      expect(component.email).toBe(expectedEmail);
      expect(component.message).toBe(expectedMessage);

      expect(spy.calls.any()).toBe(true);
      expect(page.httpSpy.calls.any()).toBe(true, "http.post called");
    });
  }));

});
