import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  email: string;
  message: string;
  successAlert = false;
  failAlert = false;

  constructor(private http: Http) { }

  ngOnInit() {
  }

  submitMessage() {
    this.http.post('api/contact', { email: this.email, message: this.message })
      .toPromise()
      .then((res) => {
        this.email = null;
        this.message = null;
        this.successAlert = true;
      })
      .catch((err) => {

      });
  }
  closeAlert(alert) {
    switch (alert) {
      case "success":
        {
          this.successAlert = false;
          break;
        }
      case "fail":
        {
          this.failAlert = false;
          break;
        }

      default:
        break;
    }
  }

}
