/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ResumePageComponent } from './resume-page.component';

xdescribe('ResumePageComponent', () => {
  let component: ResumePageComponent;
  let fixture: ComponentFixture<ResumePageComponent>;
  let page: Page;
  //	let serviceStub: Service;

  class Page {
    /** SUT HTML native elements */

    headerText: HTMLHeadingElement;

    skillsWell: HTMLDivElement;
    educationWell: HTMLDivElement;
    experienceWell: HTMLDivElement;

    constructor() {
    }

    /** Add page elements after input arrives */
    addPageElements() {

      this.headerText = fixture.debugElement.query(By.css('div.jumbotron>h1')).nativeElement;

      this.skillsWell = fixture.debugElement.query(By.css('div.well.well-lg#skills')).nativeElement;
      this.educationWell = fixture.debugElement.query(By.css('div.well.well-lg#education')).nativeElement;
      this.experienceWell = fixture.debugElement.query(By.css('div.well.well-lg#experience')).nativeElement;

      // if (component.input) {
      // have input so these elements are now in the DOM
      // this.textInput = fixture.debugElement.query(By.css('input')).nativeElement;
      // this.button = fixture.debugElement.query(By.css('button')).nativeElement;
      // }
    }
  }
  function createComponent() {

    /** Service stubs */

    // let ServiceStub = {
    // 	method: (id: number) => id
    // }

    TestBed.configureTestingModule({
      imports: [],	// FormsModule
      declarations: [ResumePageComponent],
      providers: []	// { provide: Service, useValue: ServiceStub }
    })
      .compileComponents();

    fixture = TestBed.createComponent(ResumePageComponent);
    component = fixture.componentInstance;
    page = new Page();
    // serviceStub = fixture.debugElement.injector.get(Service);

    /** Inputted Property Initialization */

    // component.property = {}



    // 1st change detection triggers ngOnInit
    fixture.detectChanges();
    return fixture.whenStable().then(() => {
      // 2nd change detection displays the async
      fixture.detectChanges();
      page.addPageElements();
    });
  }



  beforeEach(async(() => {
    createComponent();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should say "Resume" in the header', () => {
    expect(page.headerText.textContent).toContain("Resume");
  });

  it('should have Skills, Education, Experience headers in content column', () => {
    let skillsHeader = page.skillsWell.querySelector('h1.col-md-3');
    let educationHeader = page.educationWell.querySelector('h1.col-md-3');
    let experienceHeader = page.experienceWell.querySelector('h1.col-md-3');

    expect(skillsHeader.textContent).toContain('Skills');
    expect(educationHeader.textContent).toContain('Education');
    expect(experienceHeader.textContent).toContain('Experience');
  });
});
