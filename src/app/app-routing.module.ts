import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutPageComponent } from './about-page/about-page.component';
import { ResumePageComponent } from './resume-page/resume-page.component';

const routes: Routes = [
  { path: 'about', component: AboutPageComponent },
  { path: 'resume', component: ResumePageComponent },
  { path: '', redirectTo:'/about', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }