/* tslint:disable:no-unused-variable */

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterLinkStubDirective } from '../testing/router-stubs';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let linkDes;
  let links;
  let compiled;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RouterLinkStubDirective
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
    TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    // find DebugElements with an attached RouterLinkStubDirective
    linkDes = fixture.debugElement
      .queryAll(By.directive(RouterLinkStubDirective));

    // get the attached link directive instances using the DebugElement injectors
    links = linkDes
      .map(de => de.injector.get(RouterLinkStubDirective) as RouterLinkStubDirective);

    // get rendered element
    compiled = fixture.debugElement.nativeElement;
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app works!'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app works!');
  }));

  it('should render title in a navbar-brand', async(() => {
    expect(compiled.querySelector('.navbar-brand').textContent).toContain('app works!');
  }));

  it('should render a navbar toggle', async(() => {
    expect(compiled.querySelector('.navbar>.container>.navbar-header>.navbar-toggle')).toBeTruthy();
  }));

  it('should render a navbar-nav', async(() => {
    expect(compiled.querySelector('.navbar>.container>.collapse.navbar-collapse>ul.nav.navbar-nav')).toBeTruthy();
  }));

  it(`should render 'About', 'Resume', 'Projects' as tabs in a navbar-nav`, async(() => {
    expect(compiled.querySelectorAll('ul.nav.navbar-nav>li>a')[0].textContent).toContain('About');
    expect(compiled.querySelectorAll('ul.nav.navbar-nav>li>a')[1].textContent).toContain('Resume');
    expect(compiled.querySelectorAll('ul.nav.navbar-nav>li>a')[2].textContent).toContain('Projects');
  }));

  it(`should render 'Projects' as a dropdown with items 'Random Generator' and 'Stocks'`, async(() => {
    expect(compiled.querySelector('li.dropdown>a.dropdown-toggle[data-toggle="dropdown"]').textContent).toContain('Projects');
    expect(compiled.querySelector('li.dropdown>a.dropdown-toggle[data-toggle="dropdown"]>span.caret')).toBeTruthy();
    expect(compiled.querySelector('li.dropdown>ul.dropdown-menu')).toBeTruthy();
    expect(compiled.querySelectorAll('li.dropdown>ul.dropdown-menu>li>a')[0].textContent).toContain('Random Generator');
    expect(compiled.querySelectorAll('li.dropdown>ul.dropdown-menu>li>a')[1].textContent).toContain('Stocks');
  }));

  it('can get RouterLinks from template', () => {
    expect(links.length).toBe(4, 'should have 4 links');
    expect(links[0].linkParams).toBe('/about', '1st link should go to About');
    expect(links[1].linkParams).toBe('/resume', '2nd link should go to Resume');
    expect(links[2].linkParams).toBe('/random', '3nd link should go to Random Generators');
    expect(links[3].linkParams).toBe('/stocks', '4th link should go to Stocks');
  });
});
